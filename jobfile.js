var addon = require('./build/Release/jobfile');

module.exports = {
  read: addon.read,

  SCHED_S_TASK_READY: 267008,
  SCHED_S_TASK_RUNNING: 267009,
  SCHED_S_TASK_NOT_SCHEDULED: 267013
};
