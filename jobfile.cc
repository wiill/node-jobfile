#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <node.h>
#include <string>
#include <v8.h>

// Task priority
#define NORMAL_PRIORITY_CLASS   0x0020
#define IDLE_PRIORITY_CLASS     0x0040
#define HIGH_PRIORITY_CLASS     0x0080
#define REALTIME_PRIORITY_CLASS 0x0100

// Task status
#define SCHED_S_TASK_READY         0x00041300
#define SCHED_S_TASK_RUNNING       0x00041301
#define SCHED_S_TASK_NOT_SCHEDULED 0x00041305

// Task flags
#define TASK_FLAG_INTERACTIVE                  0x00000001
#define TASK_FLAG_DELETE_WHEN_DONE             0x00000002
#define TASK_FLAG_DISABLED                     0x00000004
#define TASK_FLAG_START_ONLY_IF_IDLE           0x00000010
#define TASK_FLAG_KILL_ON_IDLE_END             0x00000020
#define TASK_FLAG_DONT_START_IF_ON_BATTERIES   0x00000040
#define TASK_FLAG_KILL_IF_GOING_ON_BATTERIES   0x00000080
#define TASK_FLAG_RUN_ONLY_IF_DOCKED           0x00000100
#define TASK_FLAG_HIDDEN                       0x00000200
#define TASK_FLAG_RUN_IF_CONNECTED_TO_INTERNET 0x00000400
#define TASK_FLAG_RESTART_ON_IDLE_RESUME       0x00000800
#define TASK_FLAG_SYSTEM_REQUIRED              0x00001000
#define TASK_FLAG_RUN_ONLY_IF_LOGGED_ON        0x00002000
#define TASK_APPLICATION_NAME                  0x01000000

// Trigger flags
#define TASK_TRIGGER_FLAG_HAS_END_DATE         0x01
#define TASK_TRIGGER_FLAG_KILL_AT_DURATION_END 0x02
#define TASK_TRIGGER_FLAG_DISABLED             0x04

// Trigger types
#define ONCE                 0x00
#define DAILY                0x01
#define WEEKLY               0x02
#define MONTHLYDATE          0x03
#define MONTHLYDOW           0x04
#define EVENT_ON_IDLE        0x05
#define EVENT_AT_SYSTEMSTART 0x06
#define EVENT_AT_LOGON       0x07

#pragma pack(push, 1)
struct FIXDLEN_DATA {
  unsigned short productVersion;
  unsigned short fileVersion;
  unsigned char uuid[16];
  unsigned short appNameLenOffset;
  unsigned short triggerOffset;
  unsigned short errorRetryCount;
  unsigned short errorRetryInterval;
  unsigned short idleDeadline;
  unsigned short idleWait;
  unsigned int priority;
  unsigned int maxRunTime;
  unsigned int exitCode;
  unsigned int status;
  unsigned int flags;
  unsigned short year;
  unsigned short month;
  unsigned short weekday;
  unsigned short day;
  unsigned short hour;
  unsigned short minute;
  unsigned short second;
  unsigned short milliseconds;
};

struct TASK_TRIGGER {
  unsigned short triggerSize;
  unsigned short reserved1;
  unsigned short beginYear;
  unsigned short beginMonth;
  unsigned short beginDay;
  unsigned short endYear;
  unsigned short endMonth;
  unsigned short endDay;
  unsigned short startHour;
  unsigned short startMinute;
  unsigned int minutesDuration;
  unsigned int minutesInterval;
  unsigned int flags;
  unsigned int triggerType;
  unsigned short triggerSpecific0;
  unsigned short triggerSpecific1;
  unsigned short triggerSpecific2;
  unsigned short padding;
  unsigned short reserved2;
  unsigned short reserved3;
};
#pragma pack(pop)

struct VARLEN_DATA {
  unsigned short runningInstanceCount;

  unsigned short applicationNameLen;
  wchar_t applicationName[1024];

  unsigned short parametersLen;
  wchar_t parameters[1024];

  unsigned short workingDirectoryLen;
  wchar_t workingDirectory[1024];

  unsigned short authorLen;
  wchar_t author[1024];

  unsigned short commentLen;
  wchar_t comment[1024];

  unsigned short userDataSize;
  unsigned char userData[4];

  unsigned short reservedDataSize;
  unsigned char reservedData[8];

  unsigned short triggerCount;
  TASK_TRIGGER* triggers;

  unsigned short signatureVersion;
  unsigned short minClientVersion;
  unsigned char signature[64];
};

const struct FIXDLEN_DATA FIXDLEN_DATA_ZERO = {0};
const struct VARLEN_DATA VARLEN_DATA_ZERO = {0};

void atoh(char* buffer, const unsigned char* data, size_t len) {
  const char hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

  for (size_t i = 0; i < len; ++i) {
    const char byte = data[i];
    buffer[i * 2] = hex_chars[(byte & 0xF0) >> 4];
    buffer[i * 2 + 1] = hex_chars[(byte & 0x0F) >> 0];
  }
  buffer[len * 2] = 0;
}

bool readFile(const char* filename, FIXDLEN_DATA* fixdlen, VARLEN_DATA* varlen) {
  *fixdlen = FIXDLEN_DATA_ZERO;
  *varlen = VARLEN_DATA_ZERO;

  std::fstream file(filename, std::fstream::in | std::fstream::binary);
  if (file.is_open()) {
    file.read(reinterpret_cast<char*>(fixdlen), sizeof(FIXDLEN_DATA));

    file.read(reinterpret_cast<char*>(&varlen->runningInstanceCount), sizeof(unsigned short));

    file.read(reinterpret_cast<char*>(&varlen->applicationNameLen), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->applicationName), varlen->applicationNameLen * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->parametersLen), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->parameters), varlen->parametersLen * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->workingDirectoryLen), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->workingDirectory), varlen->workingDirectoryLen * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->authorLen), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->author), varlen->authorLen * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->commentLen), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->comment), varlen->commentLen * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->userDataSize), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->userData), varlen->userDataSize * sizeof(wchar_t));

    file.read(reinterpret_cast<char*>(&varlen->reservedDataSize), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->reservedData), varlen->reservedDataSize * sizeof(unsigned char));

    file.read(reinterpret_cast<char*>(&varlen->triggerCount), sizeof(unsigned short));
    if (varlen->triggerCount > 0) {
      varlen->triggers = new TASK_TRIGGER[varlen->triggerCount];
      for (unsigned short i = 0; i < varlen->triggerCount; ++i) {
        file.read(reinterpret_cast<char*>(&varlen->triggers[i]), sizeof(TASK_TRIGGER));
      }
    }

    file.read(reinterpret_cast<char*>(&varlen->signatureVersion), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(&varlen->minClientVersion), sizeof(unsigned short));
    file.read(reinterpret_cast<char*>(varlen->signature), sizeof(unsigned char) * 64);

    file.close();

    return true;
  } else {
    return false;
  }
}

v8::Handle<v8::Value> Read(const v8::Arguments& args) {
  v8::HandleScope scope;

  char buffer[1024] = {0};

  FIXDLEN_DATA* fixdlen = new FIXDLEN_DATA;
  VARLEN_DATA* varlen = new VARLEN_DATA;
  if (readFile(*v8::String::Utf8Value(args[0]->ToString()), fixdlen, varlen)) {
    v8::Handle<v8::Object> obj = v8::Object::New();

    obj->Set(v8::String::New("productVersion"), v8::Number::New(fixdlen->productVersion));
    obj->Set(v8::String::New("fileVersion"), v8::Number::New(fixdlen->fileVersion));

    buffer[0] = 0;
    atoh(buffer, fixdlen->uuid, 16);
    obj->Set(v8::String::New("uuid"), v8::String::New(buffer));

    obj->Set(v8::String::New("errorRetryCount"), v8::Number::New(fixdlen->errorRetryCount));
    obj->Set(v8::String::New("errorRetryInterval"), v8::Number::New(fixdlen->errorRetryInterval));
    obj->Set(v8::String::New("idleDeadline"), v8::Number::New(fixdlen->idleDeadline));
    obj->Set(v8::String::New("idleWait"), v8::Number::New(fixdlen->idleWait));

    v8::Handle<v8::Object> priority = v8::Object::New();
    priority->Set(v8::String::New("normal"), v8::Boolean::New((fixdlen->priority & NORMAL_PRIORITY_CLASS) != 0));
    priority->Set(v8::String::New("idle"), v8::Boolean::New((fixdlen->priority & IDLE_PRIORITY_CLASS) != 0));
    priority->Set(v8::String::New("high"), v8::Boolean::New((fixdlen->priority & HIGH_PRIORITY_CLASS) != 0));
    priority->Set(v8::String::New("realtime"), v8::Boolean::New((fixdlen->priority & REALTIME_PRIORITY_CLASS) != 0));
    obj->Set(v8::String::New("priority"), priority);

    obj->Set(v8::String::New("maxRunTime"), v8::Number::New(fixdlen->maxRunTime));
    obj->Set(v8::String::New("exitCode"), v8::Number::New(fixdlen->exitCode));

    v8::Handle<v8::Object> status = v8::Object::New();
    status->Set(v8::String::New("ready"), v8::Boolean::New(fixdlen->status == SCHED_S_TASK_READY));
    status->Set(v8::String::New("running"), v8::Boolean::New(fixdlen->status == SCHED_S_TASK_RUNNING));
    status->Set(v8::String::New("notScheduled"), v8::Boolean::New(fixdlen->status == SCHED_S_TASK_NOT_SCHEDULED));
    obj->Set(v8::String::New("status"), status);

    v8::Handle<v8::Object> flags = v8::Object::New();
    flags->Set(v8::String::New("interactive"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_INTERACTIVE) != 0));
    flags->Set(v8::String::New("deleteWhenDone"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_DELETE_WHEN_DONE) != 0));
    flags->Set(v8::String::New("disabled"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_DISABLED) != 0));
    flags->Set(v8::String::New("startOnlyIfIdle"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_START_ONLY_IF_IDLE) != 0));
    flags->Set(v8::String::New("killOnIdleEnd"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_KILL_ON_IDLE_END) != 0));
    flags->Set(v8::String::New("dontStartIfOnBatteries"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_DONT_START_IF_ON_BATTERIES) != 0));
    flags->Set(v8::String::New("killIfGoingOnBatteries"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_KILL_IF_GOING_ON_BATTERIES) != 0));
    flags->Set(v8::String::New("runOnlyIfDocked"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_RUN_ONLY_IF_DOCKED) != 0));
    flags->Set(v8::String::New("hidden"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_HIDDEN) != 0));
    flags->Set(v8::String::New("runIfConnectedToInternet"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_RUN_IF_CONNECTED_TO_INTERNET) != 0));
    flags->Set(v8::String::New("restartOnIdleResume"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_RESTART_ON_IDLE_RESUME) != 0));
    flags->Set(v8::String::New("systemRequired"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_SYSTEM_REQUIRED) != 0));
    flags->Set(v8::String::New("runOnlyIfLoggedOn"), v8::Boolean::New((fixdlen->flags & TASK_FLAG_RUN_ONLY_IF_LOGGED_ON) != 0));
    flags->Set(v8::String::New("hasApplicationName"), v8::Boolean::New((fixdlen->flags & TASK_APPLICATION_NAME) != 0));
    obj->Set(v8::String::New("flags"), flags);

    v8::Handle<v8::Object> lastRun = v8::Object::New();
    lastRun->Set(v8::String::New("year"), v8::Number::New(fixdlen->year));
    lastRun->Set(v8::String::New("month"), v8::Number::New(fixdlen->month));
    lastRun->Set(v8::String::New("weekday"), v8::Number::New(fixdlen->weekday));
    lastRun->Set(v8::String::New("day"), v8::Number::New(fixdlen->day));
    lastRun->Set(v8::String::New("hour"), v8::Number::New(fixdlen->hour));
    lastRun->Set(v8::String::New("minute"), v8::Number::New(fixdlen->minute));
    lastRun->Set(v8::String::New("second"), v8::Number::New(fixdlen->second));
    lastRun->Set(v8::String::New("milliseconds"), v8::Number::New(fixdlen->milliseconds));
    obj->Set(v8::String::New("lastRun"), lastRun);

    obj->Set(v8::String::New("runningInstanceCount"), v8::Number::New(varlen->runningInstanceCount));

    buffer[0] = 0;
    buffer[1] = 0;
    wcstombs(buffer, varlen->applicationName, varlen->applicationNameLen);
    obj->Set(v8::String::New("applicationName"), v8::String::New(buffer));

    buffer[0] = 0;
    buffer[1] = 0;
    wcstombs(buffer, varlen->parameters, varlen->parametersLen);
    obj->Set(v8::String::New("parameters"), v8::String::New(buffer));

    buffer[0] = 0;
    buffer[1] = 0;
    wcstombs(buffer, varlen->workingDirectory, varlen->workingDirectoryLen);
    obj->Set(v8::String::New("workingDirectory"), v8::String::New(buffer));

    buffer[0] = 0;
    buffer[1] = 0;
    wcstombs(buffer, varlen->author, varlen->authorLen);
    obj->Set(v8::String::New("author"), v8::String::New(buffer));

    buffer[0] = 0;
    buffer[1] = 0;
    wcstombs(buffer, varlen->comment, varlen->commentLen);
    obj->Set(v8::String::New("comment"), v8::String::New(buffer));

    buffer[0] = 0;
    atoh(buffer, varlen->userData, varlen->userDataSize);
    obj->Set(v8::String::New("userData"), v8::String::New(buffer));

    buffer[0] = 0;
    atoh(buffer, varlen->reservedData, varlen->reservedDataSize);
    obj->Set(v8::String::New("reservedData"), v8::String::New(buffer));

    v8::Handle<v8::Array> triggers = v8::Array::New();
    for (unsigned short i = 0; i < varlen->triggerCount; ++i) {
      v8::Handle<v8::Object> trigger = v8::Object::New();
      trigger->Set(v8::String::New("beginYear"), v8::Number::New(varlen->triggers[i].beginYear));
      trigger->Set(v8::String::New("beginMonth"), v8::Number::New(varlen->triggers[i].beginMonth));
      trigger->Set(v8::String::New("beginDay"), v8::Number::New(varlen->triggers[i].beginDay));
      trigger->Set(v8::String::New("endYear"), v8::Number::New(varlen->triggers[i].endYear));
      trigger->Set(v8::String::New("endMonth"), v8::Number::New(varlen->triggers[i].endMonth));
      trigger->Set(v8::String::New("endDay"), v8::Number::New(varlen->triggers[i].endDay));
      trigger->Set(v8::String::New("startHour"), v8::Number::New(varlen->triggers[i].startHour));
      trigger->Set(v8::String::New("startMinute"), v8::Number::New(varlen->triggers[i].startMinute));
      trigger->Set(v8::String::New("minutesInterval"), v8::Number::New(varlen->triggers[i].minutesInterval));
      trigger->Set(v8::String::New("minutesDuration"), v8::Number::New(varlen->triggers[i].minutesDuration));
      trigger->Set(v8::String::New("minutesDuration"), v8::Number::New(varlen->triggers[i].minutesDuration));
      trigger->Set(v8::String::New("minutesInterval"), v8::Number::New(varlen->triggers[i].minutesInterval));

      v8::Handle<v8::Object> flags = v8::Object::New();
      flags->Set(v8::String::New("hasEndDate"), v8::Boolean::New((varlen->triggers[i].flags & TASK_TRIGGER_FLAG_HAS_END_DATE) != 0));
      flags->Set(v8::String::New("killAtDurationEnd"), v8::Boolean::New((varlen->triggers[i].flags & TASK_TRIGGER_FLAG_KILL_AT_DURATION_END) != 0));
      flags->Set(v8::String::New("disabled"), v8::Boolean::New((varlen->triggers[i].flags & TASK_TRIGGER_FLAG_DISABLED) != 0));
      trigger->Set(v8::String::New("flags"), flags);

      switch (varlen->triggers[i].triggerType) {
        case ONCE:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("once"));
          break;
        case DAILY:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("daily"));
          break;
        case WEEKLY:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("weekly"));
          break;
        case MONTHLYDATE:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("monthlyDate"));
          break;
        case MONTHLYDOW:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("monthlyDow"));
          break;
        case EVENT_ON_IDLE:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("idle"));
          break;
        case EVENT_AT_SYSTEMSTART:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("systemStart"));
          break;
        case EVENT_AT_LOGON:
          trigger->Set(v8::String::New("triggerType"), v8::String::New("logon"));
          break;
      }

      trigger->Set(v8::String::New("triggerSpecific0"), v8::Number::New(varlen->triggers[i].triggerSpecific0));
      trigger->Set(v8::String::New("triggerSpecific1"), v8::Number::New(varlen->triggers[i].triggerSpecific1));
      trigger->Set(v8::String::New("triggerSpecific2"), v8::Number::New(varlen->triggers[i].triggerSpecific2));
      triggers->Set(i, trigger);
    }
    obj->Set(v8::String::New("triggers"), triggers);

    obj->Set(v8::String::New("signatureVersion"), v8::Number::New(varlen->signatureVersion));
    obj->Set(v8::String::New("minClientVersion"), v8::Number::New(varlen->minClientVersion));
    buffer[0] = 0;
    atoh(buffer, varlen->signature, 64);
    obj->Set(v8::String::New("signature"), v8::String::New(buffer));

    delete fixdlen;
    delete[] varlen->triggers;
    delete varlen;

    return scope.Close(obj);
  } else {
    delete fixdlen;
    delete varlen;

    return v8::ThrowException(v8::Exception::Error(v8::String::New("Failed to read file")));
  }
}

void init(v8::Handle<v8::Object> exports) {
  exports->Set(v8::String::NewSymbol("read"), v8::FunctionTemplate::New(Read)->GetFunction());
}

NODE_MODULE(jobfile, init)
